 ###### tags: `Linux`

# TP2 pt. 1 : Gestion de service
**🌞 Démarrer le service Apache**
```console
[gaby@web ~]$ sudo systemctl start httpd
[sudo] password for gaby:
[gaby@web ~]$ sudo systemctl enable httpd
[gaby@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[gaby@web ~]$ sudo firewall-cmd --reload
success
[gaby@web ~]$ sudo ss -alnpt
State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port    Process
LISTEN    0         128                0.0.0.0:22              0.0.0.0:*        users:(("sshd",pid=830,fd=5))
LISTEN    0         128                      *:80                    *:*        users:(("httpd",pid=2062,fd=4),("httpd",pid=2061,fd=4),("httpd",pid=2060,fd=4),("httpd",pid=2058,fd=4))
LISTEN    0         128                   [::]:22                 [::]:*        users:(("sshd",pid=830,fd=7))
```
**🌞 TEST**
```console
[gaby@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-10-06 11:33:51 CEST; 27min ago
     Docs: man:httpd.service(8)
 Main PID: 831 (httpd)
   Status: "Total requests: 1; Idle/Busy workers 100/0;Requests/sec: 0.000606; Bytes served/sec:   4 B/sec"
    Tasks: 213 (limit: 11397)
   Memory: 29.3M
   CGroup: /system.slice/httpd.service
           ├─831 /usr/sbin/httpd -DFOREGROUND
           ├─862 /usr/sbin/httpd -DFOREGROUND
           ├─863 /usr/sbin/httpd -DFOREGROUND
           ├─864 /usr/sbin/httpd -DFOREGROUND
           └─865 /usr/sbin/httpd -DFOREGROUND

Oct 06 11:33:50 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Oct 06 11:33:51 web.tp2.linux httpd[831]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using web.tp2.linux. Set the 'ServerName' directive globa>
Oct 06 11:33:51 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Oct 06 11:33:51 web.tp2.linux httpd[831]: Server configured, listening on: port 80
```

```console
[gaby@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
[...]
```

## 2. Avancer vers la maîtrise du service
**🌞 Le service Apache...**
- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume
```
[gaby@web ~]$ sudo systemctl enable httpd
```
- prouvez avec une commande qu'actuellement, le service est paramétré pour démarrer quand la machine s'allume
```console
[gaby@web ~]$ sudo systemctl status httpd
[sudo] password for gaby:
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
```
- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache
```console
[gaby@web ~]$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
**🌞 Déterminer sous quel utilisateur tourne le processus Apache**
- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé
```console
[gaby@web ~]$ cat /etc/httpd/conf/httpd.conf | grep User
User apache
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
```
- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```console
[gaby@web ~]$ ps -ef | grep httpd
root         834       1  0 13:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       859     834  0 13:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       860     834  0 13:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       861     834  0 13:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       862     834  0 13:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
gaby        1719    1655  0 13:19 pts/0    00:00:00 grep --color=auto httpd
```
- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
    - vérifiez avec un `ls -al` que tout son contenu est accessible en lecture à l'utilisateur mentionné dans le fichier de conf
```console
[gaby@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Oct  6 09:40 .
drwxr-xr-x. 91 root root 4096 Oct  6 09:40 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html    <- Accès à la lecture du fichier.
```
**🌞 Changer l'utilisateur utilisé par Apache**
- créez le nouvel utilisateur
```console
[gaby@web ~]$ sudo useradd webuser -m -d /usr/share/httpd -s /sbin/nologin
[sudo] password for gaby:
useradd: warning: the home directory already exists.
Not copying any file from skel directory into it.
```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```console
[gaby@web ~]$ cat /etc/httpd/conf/httpd.conf | grep -i "User\|Group"
User webuser
Group webuser
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
```
- redémarrez Apache
```console
[gaby@web ~]$ sudo systemctl restart httpd
```
- utilisez une commande `ps` pour vérifier que le changement a pris effet
```console
[gaby@web ~]$ ps -ef | grep httpd
root        1962       1  0 13:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webuser     1964    1962  0 13:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webuser     1965    1962  0 13:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webuser     1966    1962  0 13:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webuser     1967    1962  0 13:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
gaby        2189    1655  0 13:44 pts/0    00:00:00 grep --color=auto httpd
```
**🌞 Faites en sorte que Apache tourne sur un autre port**
- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix
```console
[gaby@web ~]$ cat /etc/httpd/conf/httpd.conf | grep Listen
Listen 8080
```
- ouvrez un nouveau port firewall, et fermez l'ancien
```console
[gaby@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[gaby@web ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[gaby@web ~]$ sudo firewall-cmd --reload
success
```
- redémarrez Apache
```console
[gaby@web ~]$ sudo systemctl restart httpd
```
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```console
[gaby@web ~]$ sudo ss -antpl
State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port    Process
LISTEN    0         128                0.0.0.0:22              0.0.0.0:*        users:(("sshd",pid=838,fd=5))
LISTEN    0         128                      *:8080                  *:*        users:(("httpd",pid=2331,fd=4),("httpd",pid=2330,fd=4),("httpd",pid=2329,fd=4),("httpd",pid=2326,fd=4))
LISTEN    0         128                   [::]:22                 [::]:*        users:(("sshd",pid=838,fd=7))
```
- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```console
[gaby@web ~]$ curl localhost:8080
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
      [...]
```
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port
```console
$ curl 10.102.1.11:8080
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
      [...]
```
📁 Fichier `/etc/httpd/conf/httpd.conf`

## II. Une stack web plus avancée
### A. Serveur Web et NextCloud
**🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`**
```console
sudo dnf install epel-release
sudo dnf update
sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
sudo dnf module enable php:remi-7.4
sudo dnf install httpd vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
sudo mkdir /etc/httpd/sites-available
sudo vi /etc/httpd/sites-available/tp2.linux.nextcloud
sudo mkdir /etc/httpd/sites-enabled
sudo ln -s /etc/httpd/sites-available/tp2.linux.nextcloud /etc/httpd/sites-enabled/
timedatectl
sudo vim /etc/opt/remi/php74/php.ini
wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
unzip nextcloud-22.2.0.zip
cd nextcloud/
sudo cp -Rf * /var/www/sub-domains/tp2.linux.nextcloud/html/
sudo chown -Rf apache.apache /var/www/sub-domains/tp2.linux.nextcloud
sudo systemctl restart httpd
```
📁 Fichier `/etc/httpd/conf/httpd.conf` (httpd.conf.nextcloud)
📁 Fichier `/etc/httpd/sites-available/web.tp2.linux`
### B. Base de données
**🌞 Install de MariaDB sur `db.tp2.linux`**
```console
sudo dnf install mariadb-server
sudo systemctl enable mariadb
sudo systemctl start mariadb
mysql_secure_installation
```
- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`
```console
[sudo] password for gaby:
State              Recv-Q             Send-Q                         Local Address:Port                         Peer Address:Port            Process
LISTEN             0                  128                                  0.0.0.0:22                                0.0.0.0:*                users:(("sshd",pid=832,fd=5))
LISTEN             0                  80                                         *:3306                                    *:*                users:(("mysqld",pid=4546,fd=21))
LISTEN             0                  128                                     [::]:22                                   [::]:*                users:(("sshd",pid=832,fd=7))
```
**🌞 Préparation de la base pour NextCloud**
```console
[gaby@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'nextcloud';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```
**🌞 Exploration de la base de données**
```console
[gaby@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 18
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)
```
- trouver une commande qui permet de lister tous les utilisateurs de la base de données
Sur `db.tp2.linux` :
```console
MariaDB [(none)]> SELECT User, Host FROM mysql.user;
+-----------+-------------+
| User      | Host        |
+-----------+-------------+
| nextcloud | 10.102.1.11 |
| root      | 127.0.0.1   |
| root      | ::1         |
| root      | localhost   |
+-----------+-------------+
4 rows in set (0.000 sec)
```
### C. Finaliser l'installation de NextCloud
**🌞 sur votre PC**
```console
$ cat /etc/hosts
# Host addresses
127.0.0.1  localhost
127.0.1.1  gaby-g752vs
::1        localhost ip6-localhost ip6-loopback
ff02::1    ip6-allnodes
ff02::2    ip6-allrouters
10.102.1.11 web.tp2.linux
```

**🌞 Exploration de la base de données**

- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
```console
MariaDB [(none)]> SELECT count(*) AS 'Nombre de tables' FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'nextcloud';
+------------------+
| Nombre de tables |
+------------------+
|              108 |
+------------------+
1 row in set (0.001 sec)
```

| Machine         | IP            | Service                 | Port ouvert | IP autorisées                                   |
|-----------------|---------------|-------------------------|-------------|-------------------------------------------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80/tcp      | any                                             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306/tcp    | 10.102.1.11 (Serveur web) 10.102.1.1 (Host SSH) |
