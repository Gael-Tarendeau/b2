# TP - Tarendeau Gaël - B2
## Sommaire

- [TP Réseau](https://gitlab.com/Gael-Tarendeau/b2/-/tree/main/TP-Réseau)
    - [TP1](https://gitlab.com/Gael-Tarendeau/b2/-/tree/main/TP-Réseau/TP1)
    - [TP2](https://gitlab.com/Gael-Tarendeau/b2/-/tree/main//TP-Réseau/TP2)
    - [TP3](https://gitlab.com/Gael-Tarendeau/b2/-/tree/main/TP-R%C3%A9seau/TP3)
- [TP Linux](https://gitlab.com/Gael-Tarendeau/b2/-/tree/main/TP-Linux)
    - [TP1](https://gitlab.com/Gael-Tarendeau/b2/-/tree/main/TP-Linux/TP1)
    - [TP2](https://gitlab.com/Gael-Tarendeau/b2/-/tree/main/TP-Linux/TP2)
