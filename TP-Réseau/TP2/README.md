# TP 2 - On va router des trucs

## I. ARP
### 1. Echange ARP
#### 🌞Générer des requêtes 

Sur ``node1`` :
```console
[gaby@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=1.24 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.276 ms
^C
--- 10.2.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.276/0.758/1.241/0.483 ms
[gaby@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:6a REACHABLE
10.2.1.12 dev enp0s8 lladdr 08:00:27:42:3c:f8 REACHABLE
```

Sur ``node2`` :
```console
[gaby@node2 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:6a REACHABLE
10.2.1.11 dev enp0s8 lladdr 08:00:27:46:a8:4b REACHABLE
```

L'adresse MAC de :
+ ``node1`` est ``08:00:27:46:a8:4b`` visible sur la console de ``node2``.
+ ``node2`` est ``08:00:27:42:3c:f8`` visible sur la console de ``node1``.

On verifier que tout est correcte :

Sur ``node1`` :
```console
[gaby@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:6a REACHABLE
10.2.1.12 dev enp0s8 lladdr 08:00:27:42:3c:f8 REACHABLE
```

Sur ``node2`` :
```console
[gaby@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:38:60:5e brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85096sec preferred_lft 85096sec
    inet6 fe80::a00:27ff:fe38:605e/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:42:3c:f8 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe42:3cf8/64 scope link
       valid_lft forever preferred_lft forever
```

### 2. Analyse de trames
#### 🌞Analyse de trames

On vide nos tables arp :
```console
[gaby@node1 ~]$ sudo ip n flush all
```

On utilise ``tcpdump`` sur ``node1`` et on ping avec ``node2``. On regarde ensuite sur Wireshark.

| ordre | type trame  | source                      | destination                 |
|-------|-------------|-----------------------------|-----------------------------|
|     1 | Requête ARP | `node2` `08:00:27:42:3c:f8` | `node1` `08:00:27:46:a8:4b` |
|     2 | Réponse ARP | `node1` `08:00:27:46:a8:4b` | `node2` `08:00:27:42:3c:f8` |
|     3 | Requête ARP | `node1` `08:00:27:46:a8:4b` | `node2` `08:00:27:42:3c:f8` |
|     4 | Réponse ARP | `node2` `08:00:27:42:3c:f8` | `node1` `08:00:27:46:a8:4b` |

## II. Routage
### 1. Mise en place du routage
#### 🌞Activer le routage sur le noeud ``router.2.tp2``
```console
[gaby@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[gaby@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[gaby@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
#### 🌞Ajouter les routes statiques nécessaires pour que ``node1.net1.tp2`` et ``marcel.net2.tp2`` puissent se ``ping``

Sur ``node1`` :
```console
[gaby@node1 ~]$ sudo ip route add 10.2.2.0/24 via 10.2.1.254 dev enp0s8
[sudo] password for gaby:
[gaby@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.19 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.998 ms
^C
--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.998/1.092/1.187/0.100 ms
```
Sur ``marcel`` :
```console
[gaby@marcel ~]$ sudo ip route add 10.2.1.0/24 via 10.2.2.254 dev enp0s8
[gaby@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=0.900 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=0.911 ms
^C
--- 10.2.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1006ms
rtt min/avg/max/mdev = 0.900/0.905/0.911/0.030 ms
```
### 2. Analyse de trames
#### 🌞 Analyse des échanges ARP
```console
[gaby@node1 ~]$ sudo ip n flush all
[gaby@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.29 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.17 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=0.982 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 0.982/1.148/1.293/0.133 ms
[gaby@node1 ~]$ ip n s
10.2.1.254 dev enp0s8 lladdr 08:00:27:34:51:2f REACHABLE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:6a REACHABLE
```
On remarque qu'on a ping avec ``node1`` la machine ``marcel`` or l'ip de ``marcel`` n'apparaît pas dans la table arp de ``node1`` mais celle de ``router`` oui. Donc le ping doit passer par le router pour l'envoyer à ``marcel``.

| ordre | type trame  | IP source  | MAC source                    | IP destination | MAC destination               |
|-------|-------------|------------|-------------------------------|----------------|-------------------------------|
| 1     | Requête ARP | x          | `node1` `08:00:27:46:a8:4b`   | x              | Broadcast `FF:FF:FF:FF:FF`    |
| 2     | Requête ARP | x          | `marcel` `08:00:27:ec:e0:c1`  | x              | Broadcast `FF:FF:FF:FF:FF`    |
| 3     | Réponse ARP | x          | `marcel` `08:00:27:ec:e0:c1`  | x              | `routeur` `08:00:27:46:a8:4b` |
| 4     | Réponse ARP | x          | `routeur` `08:00:27:34:51:2f` | x              | `node1` `08:00:27:46:a8:4b`   |
| 5     | Ping        | 10.2.2.12  | `marcel` `08:00:27:ec:e0:c1`  | 10.2.1.11      | `routeur` `08:00:27:46:a8:4b` |
| 6     | Ping        | 10.2.1.254 | `routeur` `08:00:27:34:51:2f` | 10.2.1.11      | `node1` `08:00:27:46:a8:4b`   |
| 7     | Pong        | 10.2.1.11  | `node1` `08:00:27:46:a8:4b`   | 10.2.1.254     | `routeur` `08:00:27:34:51:2f` |
| 8     | Pong        | 10.2.1.11  | `routeur` `08:00:27:46:a8:4b` | 10.2.2.12      | `marcel` `08:00:27:ec:e0:c1`  |

### 3. Accès internet
#### 🌞 Donnez un accès internet à vos machines
```console
[gaby@node1 ~]$ sudo !!
sudo ip route add default via 10.2.1.254 dev enp0s8
[sudo] password for gaby:
[gaby@node1 ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 1.1.1.1
[gaby@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=16.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=61 time=17.6 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1004ms
rtt min/avg/max/mdev = 16.905/17.228/17.551/0.323 ms
[gaby@node1 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37600
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             256     IN      A       216.58.198.206

;; Query time: 19 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 25 14:58:13 CEST 2021
;; MSG SIZE  rcvd: 55

[gaby@node1 ~]$ ping google.com
PING google.com (216.58.213.142) 56(84) bytes of data.
64 bytes from par21s03-in-f142.1e100.net (216.58.213.142): icmp_seq=1 ttl=61 time=15.7 ms
64 bytes from par21s03-in-f142.1e100.net (216.58.213.142): icmp_seq=2 ttl=61 time=16.5 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 15.650/16.070/16.491/0.439 ms
```
#### 🌞 Analyse de trames

| ordre | type trame | IP source           | MAC source                    | IP destination      | MAC destination               |
|-------|------------|---------------------|-------------------------------|---------------------|-------------------------------|
| 1     | ping       | `node1` `10.2.1.11` | `node1` `08:00:27:46:a8:4b`   | `8.8.8.8`           | `routeur` `08:00:27:34:51:2f` |
| 2     | pong       | `8.8.8.8`           | `routeur` `08:00:27:34:51:2f` | `node1` `10.2.1.11` | `node1` `08:00:27:46:a8:4b`   |

## III. DHCP
### 1. Mise en place du serveur DHCP
#### 🌞 Sur la machine `node1.net1.tp2`, vous installerez et configurerez un serveur DHCP
```console
[gaby@node1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for gaby: 
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
default-lease-time 600;
max-lease-time 10800;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
  range 10.2.1.12 10.2.1.253;
  option routers 10.2.1.254;
  option subnet-mask 255.255.255.0;
  option domain-name-servers 1.1.1.1;
}
```

Sur `node2` on configure l'ip en dhcp :
```console
[gaby@node2 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
[sudo] password for gaby: 
NAME=enp0s8
DEVICE=enp0s8
BOOTPROTO=dhcp
ONBOOT=yes
NETMASK=255.255.255.0
[gaby@node2 ~]$ sudo nmcli con reload
[gaby@node2 ~]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)
```
Puis on vérifie :
```console
[gaby@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:42:3c:f8 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 334sec preferred_lft 334sec
    inet6 fe80::a00:27ff:fe42:3cf8/64 scope link 
       valid_lft forever preferred_lft forever
```

#### 🌞 Améliorer la configuration du DHCP

```console
[gaby@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=1.56 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.921 ms
^C
--- 10.2.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.921/1.242/1.564/0.323 ms
[gaby@node2 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100 
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 100
gaby@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=18.5 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 18.480/18.480/18.480/0.000 ms
[gaby@node2 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 41179
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             31      IN      A       142.250.75.238

;; Query time: 18 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 25 16:42:36 CEST 2021
;; MSG SIZE  rcvd: 55

[gaby@node2 ~]$ ping gitlab.com
PING gitlab.com (172.65.251.78) 56(84) bytes of data.
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=1 ttl=61 time=19.9 ms
^C
--- gitlab.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 19.893/19.893/19.893/0.000 ms
```
