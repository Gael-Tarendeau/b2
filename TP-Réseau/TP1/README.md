# B1 Réseau 2019 - TP1
# TP1 - Mise en jambes
## I. Exploration locale en solo
## 1. Affichage d'informations sur la pile TCP/IP locale
#### Affichez les infos des cartes réseau de votre PC
```console
PS C:\Users\gaelt> ipconfig /all

Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek PCIe GBE Family Controller
   Adresse physique . . . . . . . . . . . : 70-8B-CD-1C-D0-74
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8260
   Adresse physique . . . . . . . . . . . : E4-A7-A0-43-4C-02
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::bda4:3e16:75bd:d4b4%18(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.182(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 13 septembre 2021 09:07:19
   Bail expirant. . . . . . . . . . . . . : lundi 13 septembre 2021 12:43:53
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 115648416
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-1F-5C-7A-40-70-8B-CD-1C-D0-74
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
La carte Éthernet a pour nom *Carte Ethernet Ethernet*, son adresse MAC est l'Adresse physique soit *70-8B-CD-1C-D0-74*. Elle n'a pas d'adresse ip car la carte n'est pas connecter à un réseau.
La carte réseau WiFi a pour nom *Carte réseau sans fil Wi-Fi*, son adresse MAC est l'Adresse physique soit *E4-A7-A0-43-4C-02*. Son adresse ip est 10.33.0.182 .

#### Affichez votre gateway

```console
PS C:\Users\gaelt> ipconfig

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::bda4:3e16:75bd:d4b4%18
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.182
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```
La passerelle de la carte Wi-Fi est  la *Passerelle par défaut* soit 10.33.3.253 .

### En graphique (GUI : Graphical User Interface)
#### Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

![](https://i.imgur.com/zdDjYA3.png)

#### Questions
- À quoi sert la gateway dans le réseau d'YNOV ?

-> La gateway dans le réseau d'YNOV sert à se connecter à d'autres réseaux.

### 2. Modifications des informations
#### A. Modification d'adresse IP (part 1)
![](https://i.imgur.com/BJjS6H3.png)

On vérifie ensuite.
```console
PS C:\Users\gaelt> ipconfig

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::bda4:3e16:75bd:d4b4%18
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.103
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

#### **Il est possible que vous perdiez l'accès internet.** Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.
-> On perd l'accès internet car l'ip est déjà utilisé dans le réseau.

### B. Table ARP
#### depuis la ligne de commande, afficher la table ARP
```console
PS C:\Users\gaelt> arp -a

Interface : 192.168.56.1 --- 0x3f
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.1.10 --- 0x47
  Adresse Internet      Adresse physique      Type
  10.10.1.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
#### identifier l'adresse MAC de la passerelle de votre réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement

On regarde l'ip de la carte Wi-Fi avec la commande ``ipconfig``
```console
PS C:\Users\gaelt> ipconfig

Configuration IP de Windows

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::bda4:3e16:75bd:d4b4%19
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.182
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

Puis on cherche avec la table ARP l'adresse MAC (adresse physique) de notre passerelle par défaut.
```console
PS C:\Users\gaelt> arp /a /n 10.33.0.182

Interface : 10.33.0.182 --- 0x13
  Adresse Internet      Adresse physique      Type
  10.33.0.1             3c-58-c2-9d-98-38     dynamique
  10.33.0.6             84-5c-f3-80-32-07     dynamique
  10.33.0.7             9c-bc-f0-b6-1b-ed     dynamique
  10.33.0.19            d4-a3-3d-c8-ca-67     dynamique
  10.33.0.21            b0-fc-36-ce-9c-89     dynamique
  10.33.0.43            2c-8d-b1-94-38-bf     dynamique
  10.33.0.48            54-4e-90-3c-33-3f     dynamique
  10.33.0.57            f0-18-98-8c-6f-cd     dynamique
  10.33.0.60            e2-ee-36-a5-0b-8a     dynamique
  10.33.0.71            f0-03-8c-35-fe-47     dynamique
  10.33.0.78            ec-63-d7-c7-51-87     dynamique
  10.33.0.96            ca-4f-f4-af-8f-0c     dynamique
  10.33.0.100           e8-6f-38-6a-b6-ef     dynamique
  10.33.0.111           d2-41-f0-dc-6a-ed     dynamique
  10.33.0.135           f8-5e-a0-06-40-d2     dynamique
  10.33.0.140           40-ec-99-8b-11-c2     dynamique
  10.33.0.143           f0-18-98-41-11-07     dynamique
  10.33.0.180           26-91-29-98-e2-9d     dynamique
  10.33.0.211           e8-d0-fc-ef-9e-af     dynamique
  10.33.0.212           48-2c-a0-c8-f1-dc     dynamique
  10.33.0.217           76-8f-9c-ed-ec-b1     dynamique
  10.33.0.228           7c-5c-f8-2d-40-42     dynamique
  10.33.0.242           74-4c-a1-51-1e-61     dynamique
  10.33.0.250           28-cd-c4-dd-db-73     dynamique
  10.33.0.251           48-e7-da-41-bf-e1     dynamique
  10.33.0.253           cc-f9-e4-c5-a3-32     dynamique
  10.33.3.80            3c-58-c2-9d-98-38     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique <-----
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

L'adresse MAC du la passerel du réseau est donc ``00-12-00-40-4c-bf``.

- envoyez des ping vers des IP du même réseau que vous. Lesquelles ? menfou, random. Envoyez des ping vers au moins 3-4 machines.
- affichez votre table ARP
- listez les adresses MAC associées aux adresses IP que vous avez ping
- \+ C.``nmap``

Je regarde la table ARP.
```console
PS C:\Users\gaelt> arp -a -n 10.33.0.182

Interface : 10.33.0.182 --- 0x13
  Adresse Internet      Adresse physique      Type
  10.33.0.119           18-56-80-70-9c-48     dynamique
  10.33.1.10            b0-7d-64-b1-98-d3     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

Je fais un scan avec nmap sur le réseau puis je regarde ma table ARP.

![](https://i.imgur.com/sIAFs11.png)
```console
PS C:\Users\gaelt> arp -a -n 10.33.0.182

Interface : 10.33.0.182 --- 0x13
  Adresse Internet      Adresse physique      Type
  10.33.0.6             84-5c-f3-80-32-07     dynamique
  10.33.0.7             9c-bc-f0-b6-1b-ed     dynamique
  10.33.0.34            ac-67-5d-83-9f-e6     dynamique
  10.33.0.60            e2-ee-36-a5-0b-8a     dynamique
  10.33.0.96            ca-4f-f4-af-8f-0c     dynamique
  10.33.0.111           d2-41-f0-dc-6a-ed     dynamique
  10.33.0.119           18-56-80-70-9c-48     dynamique
  10.33.0.148           e6-aa-26-ee-23-b7     dynamique
  10.33.0.180           26-91-29-98-e2-9d     dynamique
  10.33.0.221           84-c5-a6-eb-c3-a5     dynamique
  10.33.1.10            b0-7d-64-b1-98-d3     dynamique
  10.33.1.62            b0-7d-64-b1-98-d3     dynamique
  10.33.1.63            b0-7d-64-b1-98-d3     dynamique
  10.33.1.70            30-57-14-94-de-fb     dynamique
  10.33.1.108           d6-a8-14-09-97-73     dynamique
  10.33.1.242           34-7d-f6-5a-20-da     dynamique
  10.33.1.243           34-7d-f6-5a-20-da     dynamique
  10.33.2.81            50-eb-71-d6-4e-8f     dynamique
  10.33.2.105           ec-2e-98-ca-da-e9     dynamique
  10.33.2.116           e2-be-82-4d-c5-8f     dynamique
  10.33.2.157           08-d2-3e-62-c0-dc     dynamique
  10.33.2.196           08-71-90-87-b9-8c     dynamique
  10.33.2.216           08-d2-3e-35-00-a2     dynamique
  10.33.2.225           e4-5e-37-87-f9-f1     dynamique
  10.33.3.5             74-d8-3e-0d-06-b0     dynamique
  10.33.3.59            02-47-cd-3d-d4-e9     dynamique
  10.33.3.80            3c-58-c2-9d-98-38     dynamique
  10.33.3.138           bc-54-2f-0b-53-1a     dynamique
  10.33.3.139           34-cf-f6-37-2c-fb     dynamique
  10.33.3.148           bc-76-5e-d2-75-86     dynamique
  10.33.3.168           12-88-c1-71-b7-a0     dynamique
  10.33.3.187           96-fd-87-13-4b-ee     dynamique
  10.33.3.189           c2-6f-43-3d-c7-fa     dynamique
  10.33.3.214           6c-40-08-bc-78-92     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

On peut voir les adresses MAC des nouvelles ip en regardant l'adresse physique sur le terminal.

### D. Modification d'adresse IP (part 2)
- utilisez un ping scan sur le réseau YNOV
- montrez moi la commande ``nmap`` et son résultat

![](https://i.imgur.com/JaLz76b.png)
- configurez correctement votre gateway pour avoir accès à d'autres réseaux (utilisez toujours la gateway d'YNOV)

![](https://i.imgur.com/eRNOlwy.png)
- prouvez en une suite de commande que vous avez une IP choisi manuellement, que votre passerelle est bien définie, et que vous avez un accès internet
```console
PS C:\Users\gaelt> ipconfig

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::bda4:3e16:75bd:d4b4%19
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.184
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
PS C:\Users\gaelt> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=98 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=29 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=48 ms TTL=58

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 29ms, Maximum = 98ms, Moyenne = 58ms
Ctrl+C
```

## II. Exploration locale en duo

### Cette partie a été effectué avec Thomas Curmi et Dorian Levée.

### 3. Modification d'adresse IP
- modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

![](https://i.imgur.com/vSzisba.png)
- vérifiez à l'aide de commandes que vos changements ont pris effet
```console
PS C:\Users\gaelt> ipconfig

Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::9529:33c9:aa03:2526%16
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . :
```
- utilisez ``ping`` pour tester la connectivité entre les deux machines
```console
PS C:\Users\gaelt> ping 192.168.0.2

Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=64

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 1ms, Moyenne = 1ms
```
- affichez et consultez votre table ARP
```console
PS C:\Users\gaelt> arp -a -n 192.168.0.1

Interface : 192.168.0.1 --- 0x10
  Adresse Internet      Adresse physique      Type
  192.168.0.2           00-e0-4c-68-05-25     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

### 5. Petit chat privé
#### **Sur le PC serveur (IP 192.168.0.1)**
```console
PS E:\netcat-1.11> .\nc64.exe -l -p 8888
bonjour ├á tous
c'est un super test
PS E:\netcat-1.11>
```

#### **Pour aller un peu plus loin**.
```console
PS E:\netcat-1.11> .\nc64.exe -l -p 8888 192.168.0.2
re
cela fonctionne ├®galement
PS E:\netcat-1.11>
```
### 6. Firewall
#### Autoriser les ping
- J'active mon firewall

![](https://i.imgur.com/T6HjpnD.png)

- Je crée une règle pour autoriser les ``ping``.

![](https://i.imgur.com/ZNR0U2A.png)

- Sur le terminal de Thomas, il a pu me ping :
```console
thomas@pop-os:~$ ping 192.168.0.1
PING 192.168.0.1 (192.168.0.1) 56(84) bytes of data.
64 bytes from 192.168.0.1: icmp_seq=1 ttl=128 time=1.39 ms
64 bytes from 192.168.0.1: icmp_seq=2 ttl=128 time=1.64 ms
^C
--- 192.168.0.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.394/1.517/1.641/0.123 ms
```
#### Autoriser le traffic sur le port qu'utilise ``nc``
Après avoir essayer de créer une règle sur un port via ligne de commande ou via l'interface graphique, on a pas réussi à établir une connexion avec netcat.

## III. Manipulations d'autres outils/protocoles côté client
```console
PS C:\Users\gaelt> ipconfig -all

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Intel(R) Dual Band Wireless-AC 8260
   Adresse physique . . . . . . . . . . . : E4-A7-A0-43-4C-02
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::bda4:3e16:75bd:d4b4%19(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.182(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : vendredi 17 septembre 2021 15:09:21
   Bail expirant. . . . . . . . . . . . . : vendredi 17 septembre 2021 17:05:30
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 115648416
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-1F-5C-7A-40-70-8B-CD-1C-D0-74
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
### 1. DHCP
#### Exploration du DHCP, depuis votre PC
```console
 Serveur DHCP : 10.33.3.254
 Bail expirant : vendredi 17 septembre 2021 17:05:30
```

### 2. DNS
#### Trouver l'adresse IP du serveur DNS que connaît votre ordinateur
```console
 Serveurs DNS : 10.33.10.2
. . . .  . . . . . . . . . 10.33.10.148
. . . .  . . . . . . . . . 10.33.10.155
```
#### Utiliser, en ligne de commande l'outil ``nslookup`` pour faire des requêtes DNS à la main
```console
PS C:\Users\gaelt> nslookup google.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:807::200e
          172.217.18.206

PS C:\Users\gaelt> nslookup ynov.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```
On passe toujours par le serveur DNS (visible par la ligne ``Address : 10.33.10.2``) quand on veut accéder aux sites ``google.com`` ou ``ynov.com`` et on obtient leur ip.

```console
PS C:\Users\gaelt> nslookup 78.74.21.21
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21

PS C:\Users\gaelt> nslookup 92.146.54.88
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
On passe toujours par le serveur DNS et maintenant on obtient le nom du domaine.

## IV. Wireshark
### Cette partie a été effectué avec Thomas Curmi, Dorian Levée et Benjamin Gélineau
#### Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
- un ``ping`` entre vous et la passerelle
![](https://i.imgur.com/fc8mAeP.png)
- un ``netcat`` entre vous et votre mate, branché en RJ45
![](https://i.imgur.com/kSlGTpJ.png)
- une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
![](https://i.imgur.com/HxtCLJ3.png)
