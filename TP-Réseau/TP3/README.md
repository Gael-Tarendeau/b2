###### tags: `Réseaux`
# TP3 : Progressons vers le réseau d'infrastructure

# I. (mini)Architecture réseau
## 1. Adressage
🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :
| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|---------------|-------------------|-------------------|-----------------------------|--------------------|-------------------|
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126                         | `10.3.0.126`       | `10.3.0.127`      |
| `client1`     | `10.3.0.128`      | `255.255.255.192` | 62                          | `10.3.0.190`       | `10.3.0.191`      |
| `server2`     | `10.3.0.192`      | `255.255.255.240` | 14                          | `10.3.0.206`       | `10.3.0.207`      |

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**
| Nom machine          | Adresse de la machine dans le réseau `client1` | Adresse dans `server1` | Adresse dans `server2` | Adresse de passerelle |
|----------------------|------------------------------------------------|------------------------|------------------------|-----------------------|
| `router.tp3`         | `10.3.0.190/26`                                | `10.3.0.126/25`        | `10.3.0.206/28`        | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.0.130/26`                                | X                      | X                      | `10.3.0.190/26`       |
| `marcel.client1.tp3` | DHCP `/26`                                     | X                      | X                      | `10.3.0.190/26`       |
| `johnny.client1.tp3` | DHCP `/26`                                     | X                      | X                      | `10.3.0.190/26`       |
| `dns1.server1.tp3`   | X                                              | `10.3.0.2/25`          | X                      | `10.3.0.126/25`       |
| `web1.server2.tp3`   | X                                              | X                      | `10.3.0.194/28`        | `10.3.0.206/28`       |
| `nfs1.server2.tp3`   | X                                              | X                      | `10.3.0.195/28`        | `10.3.0.206/28`       |

## 2. Routeur
🌞 **Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**
- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle
- il a un accès internet
- il a de la résolution de noms
- il porte le nom `router.tp3`*
- n'oubliez pas d'activer le routage sur la machine
```console
[gaby@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4d:a9:83 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86173sec preferred_lft 86173sec
    inet6 fe80::a00:27ff:fe4d:a983/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e4:05:90 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.126/25 brd 10.3.0.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee4:590/64 scope link 
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d5:24:77 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.190/26 brd 10.3.0.191 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fed5:2477/64 scope link 
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:63:e0:39 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.206/28 brd 10.3.0.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe63:e039/64 scope link 
       valid_lft forever preferred_lft forever
[gaby@router ~]$ hostname
router.tp3
[gaby@router ~]$ ping google.com
PING google.com (172.217.18.206) 56(84) bytes of data.
64 bytes from par10s38-in-f14.1e100.net (172.217.18.206): icmp_seq=1 ttl=63 time=17.10 ms
^C
--- google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 17.956/17.956/17.956/0.000 ms
[gaby@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
# II. Services d'infra
## 1. Serveur DHCP
🌞 **Mettre en place une machine qui fera office de serveur DHCP** dans le réseau `client1`. Elle devra :
- porter le nom `dhcp.client1.tp3`
- 📝**checklist**📝
- donner une IP aux machines clients qui le demande
- leur donner l'adresse de leur passerelle
- leur donner l'adresse d'un DNS utilisable

📁 **Fichier `dhcpd.conf`**

---

🌞 **Mettre en place un client dans le réseau `client1`**

- de son p'tit nom `marcel.client1.tp3`
- 📝**checklist**📝
- la machine récupérera une IP dynamiquement grâce au serveur DHCP
- ainsi que sa passerelle et une adresse d'un DNS utilisable

```console
[gaby@marcel ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:be:e3:cd brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.131/26 brd 10.3.0.191 scope global dynamic noprefixroute enp0s8
       valid_lft 710sec preferred_lft 710sec
    inet6 fe80::a00:27ff:febe:e3cd/64 scope link 
       valid_lft forever preferred_lft forever
[gaby@marcel ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
NAME=enp0s8
DEVICE=enp0s8
BOOTPROTO=dhcp
ONBOOT=yes
[gaby@marcel ~]$ ip r s
default via 10.3.0.190 dev enp0s8 proto dhcp metric 100 
10.3.0.128/26 dev enp0s8 proto kernel scope link src 10.3.0.131 metric 100
```

🌞 **Depuis `marcel.client1.tp3`**

- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP
- à l'aide de la commande `traceroute`, prouver que `marcel.client1.tp3` passe par `router.tp3` pour sortir de son réseau

```console
[gaby@marcel ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37525
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; Query time: 54 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Oct 02 17:29:28 CEST 2021
;; MSG SIZE  rcvd: 53
```

## 2. Serveur DNS

🌞 **Mettre en place une machine qui fera office de serveur DNS**
```bash 
#    /etc/named.conf

zone "server1.tp3" IN {
        type master;
        file "server1.tp3.forward";
        allow-update { none; };
        allow-query { any; };
};

zone "server2.tp3" IN {
        type master;
        file "server2.tp3.forward";
        allow-update { none; };
        allow-query { any; };
};
```

```bash 
#    /var/named/server1.tp3.forward

$TTL 86400
@ IN SOA dns1.server1.tp3. admin.server1.tp3 (
        2021100300 ;Serial
        3600 ;Refresh
        1800 ;Retry
        604800 ;Expire
        86400 ;Minimum TTL
) 

;Name Server Information
@ IN NS dns1.server1.tp3.

;IP for Name Server
dns1 IN A 10.3.0.2

;A Record for IP address to Hostname
router IN A 10.3.0.126
```

🌞 **Tester le DNS depuis `marcel.client1.tp3`**
```console
[gaby@marcel ~]$ dig router.server1.tp3 @10.3.0.2

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> router.server1.tp3 @10.3.0.2
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 40176
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: d3a0fe1545d9ca166b1bfad36159bdfdcae58df6f053db19 (good)
;; QUESTION SECTION:
;router.server1.tp3.            IN      A

;; ANSWER SECTION:
router.server1.tp3.     86400   IN      A       10.3.0.126

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; ADDITIONAL SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.2

;; Query time: 2 msec
;; SERVER: 10.3.0.2#53(10.3.0.2)
;; WHEN: Sun Oct 03 16:28:12 CEST 2021
;; MSG SIZE  rcvd: 126

```
**🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds**
```bash
[gaby@dhcp ~]$ sudo !!
sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for gaby: 
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
authoritative;
subnet 10.3.0.128 netmask 255.255.255.192 {
  range 10.3.0.131 10.3.0.189;
  option routers 10.3.0.190;
  option subnet-mask 255.255.255.192;
  option domain-name-servers 10.3.0.2;
}
```

```bash
[gaby@marcel ~]$ cat /etc/resolv.conf 
# Generated by NetworkManager
search client1.tp3
nameserver 10.3.0.2
```
## 3. Get deeper
### A. DNS forwarder
**🌞 Affiner la configuration du DNS**
```bash 
# dns1.server1.tp3 : /etc/named.conf
    recursion yes;
```

```console
[gaby@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8529
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 696f0e5acb5accaddf8954af615c1a524d99db87c0b87513 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             238     IN      A       216.58.214.174

;; AUTHORITY SECTION:
google.com.             172738  IN      NS      ns3.google.com.
google.com.             172738  IN      NS      ns4.google.com.
google.com.             172738  IN      NS      ns2.google.com.
google.com.             172738  IN      NS      ns1.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172738  IN      A       216.239.34.10
ns1.google.com.         172738  IN      A       216.239.32.10
ns3.google.com.         172738  IN      A       216.239.36.10
ns4.google.com.         172738  IN      A       216.239.38.10
ns2.google.com.         172738  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172738  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172738  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172738  IN      AAAA    2001:4860:4802:38::a

;; Query time: 1 msec
;; SERVER: 10.3.0.2#53(10.3.0.2)
;; WHEN: Tue Oct 05 11:26:42 CEST 2021
;; MSG SIZE  rcvd: 331

```
### B. On revient sur la conf du DHCP
**🌞 Affiner la configuration du DHCP**
```bash
[gaby@dhcp ~]$ sudo !!
sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for gaby: 
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
authoritative;
subnet 10.3.0.128 netmask 255.255.255.192 {
  range 10.3.0.131 10.3.0.189;
  option routers 10.3.0.190;
  option subnet-mask 255.255.255.192;
  option domain-name-servers 10.3.0.2;
}
```

```console
[gaby@johnny ~]$ ping router.server1.tp3
PING router.server1.tp3 (10.3.0.126) 56(84) bytes of data.
64 bytes from 10.3.0.126 (10.3.0.126): icmp_seq=1 ttl=64 time=0.726 ms
64 bytes from 10.3.0.126 (10.3.0.126): icmp_seq=2 ttl=64 time=0.644 ms
^C
--- router.server1.tp3 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.644/0.685/0.726/0.041 ms
```

# III. Services métier
**🌞 Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**
```console
[gaby@web1 ~]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-10-05 12:23:07 CEST; 8s ago
 Main PID: 23886 (python3)
    Tasks: 1 (limit: 11397)
   Memory: 9.6M
   CGroup: /system.slice/web.service
           └─23886 /bin/python3 -m http.server 8888

Oct 05 12:23:07 web1.server2.tp3 systemd[1]: Started Very simple web service.
```

**🌞 Test test test et re-test**
```console
[gaby@marcel ~]$ curl 10.3.0.194:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
## 2. Partage de fichiers
### B. Le setup wola
**🌞 Setup d'une nouvelle machine, qui sera un serveur NFS**
```console
# dans /etc/idmapd.conf sur la machine nfs1.server2.tp3
Domain = dns1.server1.tp3

[gaby@nfs1 ~]$ cat /etc/exports 
/srv/nfs_share/ 10.3.0.192/28(rw,no_root_squash)

[gaby@nfs1 ~]$ sudo firewall-cmd --add-service=nfs
success
[gaby@nfs1 ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
[gaby@nfs1 ~]$ sudo firewall-cmd --reload
success
```
**🌞 Configuration du client NFS**
```console
# dans /etc/idmapd.conf sur la machine web1.server2.tp3
Domain = dns1.server1.tp3

[gaby@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share /srv/nfs
[gaby@web1 ~]$ df -hT
Filesystem                      Type      Size  Used Avail Use% Mounted on
devtmpfs                        devtmpfs  891M     0  891M   0% /dev
tmpfs                           tmpfs     909M     0  909M   0% /dev/shm
tmpfs                           tmpfs     909M  8.5M  901M   1% /run
tmpfs                           tmpfs     909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root             xfs       6.2G  2.1G  4.2G  34% /
/dev/sda1                       xfs      1014M  241M  774M  24% /boot
tmpfs                           tmpfs     182M     0  182M   0% /run/user/1000
nfs1.server2.tp3:/srv/nfs_share nfs4      6.2G  2.2G  4.1G  35% /srv/nfs
```
**🌞 TEEEEST**
```console
[gaby@web1 ~]$ sudo vim /srv/nfs/test
[sudo] password for gaby: 
[gaby@web1 ~]$ cat /srv/nfs/test
coucou o/
```
On regarde sur `nfs1.server2.tp3` :
```console
[gaby@nfs1 ~]$ cat /srv/nfs_share/test
coucou o/
```
# IV. Un peu de théorie : TCP et UDP
**🌞 Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :**
- SSH = TCP
- HTTP = TCP
- DNS = UDP
- NFS = TCP

📁 Captures réseau `tp3_ssh.pcap`, `tp3_http.pcap`, `tp3_dns.pcap` et `tp3_nfs.pcap`

**🌞 Capturez et mettez en évidence un 3-way handshake**

📁 Capture réseau `tp3_3way.pcap`
- Visible sur les 3 premières lignes de la capture.

# V. El final
**🌞 Bah j'veux un schéma.**

![Schema_tp3](./Schéma/Schema_TP3.png "Schéma TP3")

**Tableau des réseaux :**
| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|---------------|-------------------|-------------------|-----------------------------|--------------------|-------------------|
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126                         | `10.3.0.126`       | `10.3.0.127`      |
| `client1`     | `10.3.0.128`      | `255.255.255.192` | 62                          | `10.3.0.190`       | `10.3.0.191`      |
| `server2`     | `10.3.0.192`      | `255.255.255.240` | 14                          | `10.3.0.206`       | `10.3.0.207`      |

**Tableau d'adressage :**
| Nom machine          | Adresse de la machine dans le réseau `client1` | Adresse dans `server1` | Adresse dans `server2` | Adresse de passerelle |
|----------------------|------------------------------------------------|------------------------|------------------------|-----------------------|
| `router.tp3`         | `10.3.0.190/26`                                | `10.3.0.126/25`        | `10.3.0.206/28`        | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.0.130/26`                                | X                      | X                      | `10.3.0.190/26`       |
| `marcel.client1.tp3` | DHCP `/26`                                     | X                      | X                      | `10.3.0.190/26`       |
| `johnny.client1.tp3` | DHCP `/26`                                     | X                      | X                      | `10.3.0.190/26`       |
| `dns1.server1.tp3`   | X                                              | `10.3.0.2/25`          | X                      | `10.3.0.126/25`       |
| `web1.server2.tp3`   | X                                              | X                      | `10.3.0.194/28`        | `10.3.0.206/28`       |
| `nfs1.server2.tp3`   | X                                              | X                      | `10.3.0.195/28`        | `10.3.0.206/28`       |

**🌞 Et j'veux des fichiers aussi, tous les fichiers de conf du DNS**
- 📁 Fichiers de zone (`Config dns/tp3_fichier_zone_server`)
- 📁 Fichier de conf principal DNS `named.conf` (`Config dns/tp3_named.conf`)
- [📁 Clique ici pour accéder directement au dossier](https://gitlab.com/Gael-Tarendeau/b2/-/tree/main/TP-R%C3%A9seau/TP3/Config%20dns)
